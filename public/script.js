$(document).ready(function(){
    $("form#calculForm").on('submit', function(e){
        e.preventDefault();
        var data = {nombre: $('input[name=nombre]').val()}
        console.log(data)
        $.ajax({
            type: 'post',
            url: '/',
            data: data,
            dataType: 'text',
            success: function(data){
                let parsed = JSON.parse(data)
                console.log(parsed)
                $('#resultat').html(parsed.nombre + '! = ' + parsed.resultat);
            },
            failure: function(data) {
                console.log('Failed')
                console.log(data)
            }
        });
    });
});